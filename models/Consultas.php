<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consultas".
 *
 * @property int $id
 * @property string|null $texto
 * @property string|null $tabla
 * @property string|null $sql
 * @property string|null $resultados
 */
class Consultas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consultas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto', 'tabla', 'sql', 'resultados'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto' => 'Texto',
            'tabla' => 'Tabla',
            'sql' => 'Sql',
            'resultados' => 'Resultados',
        ];
    }
}
