<?php

use app\models\Emple;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fas fa-plus-circle"></i> Nuevo empleado', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            //'salario',
            //'comision',
            //'dept_no',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Emple $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'emp_no' => $model->emp_no]);
                }
            ],
        ],
    ]); ?>


</div>