<?php

use app\models\Consultas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Consultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consultas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fas fa-plus-circle"></i> Nueva consulta', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('PDF', 'pdf', ['class' => 'btn btn-primary ', 'target' => '_blank',])  ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'texto',
            'tabla',
            'sql',
            [
                'attribute' => 'Resultados',
                'format' => 'raw',
                'value' => function ($model) {
                    return \yii\helpers\Html::a('Ver Consulta', ['consultas/mostrarconsulta', 'consulta' => $model->sql],  ["class" => "btn btn-danger"]);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Consultas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>