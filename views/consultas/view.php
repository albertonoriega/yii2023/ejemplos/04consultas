<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Consultas $model */

$this->title = 'Consulta ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Consultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="consultas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que deseas eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'texto',
            'tabla',
            'sql',
            [
                'attribute' => 'Ver consulta',
                'format' => 'raw',
                'value' =>  function ($dato) {
                    return Html::a('Ver Consulta', ['consultas/mostrarconsulta', 'consulta' => $dato->sql],  ['class' => 'btn btn-danger']);
                }

            ],
        ],
    ]) ?>

</div>