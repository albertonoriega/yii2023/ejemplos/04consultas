<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
]);

?>

<div>
    <?= Html::a('Volver a consultas', 'index', ['class' => 'btn btn-warning']) ?>
</div>