﻿DROP DATABASE IF EXISTS noticias2023;

CREATE DATABASE noticias2023;
USE noticias2023;

CREATE TABLE noticia(
idNoticia int,
titular varchar (255),
textoCorto varchar (800),
textoLargo longtext,
portada boolean,
seccion int,
fecha datetime,
foto varchar(255),
autor int,
PRIMARY KEY (idNoticia)
);