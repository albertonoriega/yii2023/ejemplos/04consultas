<?php

/** @var yii\web\View $this */

$this->title = 'Pagina de inicio';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Aplicación de consultas</h1>

    </div>

    <div class="body-content">

        <div class="row">
            <p class="lead">Tenemos tres tablas en la base de datos: <span class="rojo">consultas, emple y depart </span>. En las tres creamos el <span class="negrita">modelo</span> y el <span class="negrita">CRUD</span></p>
        </div>

        <div class="row mt-5">
            <div class="col-lg-4 mb-3">
                <p class="lead">En <span class="rojo">consultas</span> introducimos la sentencia de SQL para ejecutar la consulta de la descripción. Ademas introducimos un botón que ejecuta la consulta y vemos el resultado de la misma</p>
            </div>
            <div class="col-lg-4 mb-3">

                <p class="lead">En <span class="rojo">emple</span> vemos a través de un GridView los datos de los empleados</p>

            </div>
            <div class="col-lg-4">
                <p class="lead">En <span class="rojo">depart</span> vemos a través de un GridView los datos de los departamentos</p>
            </div>
        </div>

    </div>
</div>