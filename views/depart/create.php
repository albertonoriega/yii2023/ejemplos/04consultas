<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Depart $model */

$this->title = 'Nuevo departamento';
$this->params['breadcrumbs'][] = ['label' => 'Departamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depart-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>